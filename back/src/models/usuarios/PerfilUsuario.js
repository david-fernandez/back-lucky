const mongoose = require ('mongoose');
const Schema = mongoose.Schema;


const perfilUsuarioSchema = new Schema(
  {
    nombreApellidos : { type:String , required: true },
    email : {type:String , required: true},
    contraseña:{type:String , require: true},
    telefono : { type : String ,required : true },
    dni : { type: String , required: true },
    direccion : { type: String , required : true },
    codigoPostal : { type: String , required: true },
    ciudad : { type : String , required: true },
    imagen : { type : String,default:"https://static3.abc.es/media/play/2018/04/03/kiko-chavo-ocho-kTnH--620x349@abc.jpg"}
  },
  {
    timestamps : true,
  }
);




const PerfilUsuario = mongoose.model('PerfilUsuario' , perfilUsuarioSchema );
module.exports = PerfilUsuario;