const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Esquema principal
const datosMascotaSchema = new Schema(
  {
    nombre: { type: String, required: true },
    sexo: { type: String, required: true },
    ciudad: { type: String, required: true },
    especie: { type: String, required: true },
    // Añadido tipo que seria com la raza dentro de la especie EJ ---> especie: perro, tipo: pastor Aleman
    tipo: { type: String, required: true },
    historia: { type: String, required: true },
    fechaDeNacimiento: { type: Date, default: Date.now() },
    tamaño: { type: String, required: true },
    peso: { type: Number, required: true },
    img: { type: String, required: true },

    
    salud: {
      sano : { type: Boolean , required : true },
      esterilizado : { type: Boolean , required: true },
      vacunado : { type: Boolean , required : true },
      desparasitado : { type: Boolean , required : true },
      identificado : { type : Boolean , required : true},
      microchip: { type: Boolean , required : true },
    },
    
    adopcion: {
      requisitos: { type: String, required: true },
      tasas: { type: Number, required: true },
      queCiudad: { type: String, required: true },
    }
  },
  {
    timestamps: true,
  }
);

/* 
"sexo":"hembra",
"especie":"periquito", 
"historia":"fue recogido en la calle",
"fecheDeNacimiento": 2020-09-17,
"tamaño":"quince por veinte cm",
"peso":230
*/

const DatosMascota = mongoose.model('DatosMascotas', datosMascotaSchema);
module.exports = DatosMascota;

