const express = require('express');
const DatosMascotas = require('../../models/mascotas/DatosMascotas');
const authenticateJWT = require('../../middlewares/autentication');
const datosMascotasRouter = express.Router();


datosMascotasRouter.post('/', (req, res) => {

    const nombre = req.body.nombre;
    const sexo = req.body.sexo;
    const ciudad = req.body.ciudad;
    const especie = req.body.especie;
    // Añado tipo
    const tipo = req.body.tipo;
    const historia = req.body.historia;
    const fechaDeNacimiento = req.body.fechaDeNacimiento;
    const tamaño = req.body.tamaño;
    const peso = req.body.peso;
    const img =req.body.img;
    
    const salud = req.body.salud;
    const adopcion = req.body.adopcion;

    const datosMascotas = new DatosMascotas()
    datosMascotas.img =img;
    datosMascotas.nombre = nombre;
    datosMascotas.sexo = sexo;
    datosMascotas.ciudad = ciudad;
    datosMascotas.especie = especie;
    // Añado tipo
    datosMascotas.tipo = tipo;
    datosMascotas.historia = historia;
    datosMascotas.fechaDeNacimiento = fechaDeNacimiento;
    datosMascotas.tamaño = tamaño;
    datosMascotas.peso = peso;

    datosMascotas.salud = salud;
    datosMascotas.adopcion = adopcion;

    datosMascotas.save()
        .then((newDatoMascotas) => {
            res.json(newDatoMascotas);
        })
        .catch((error) => {
            res.status(500).send(error);
        })
});




datosMascotasRouter.get('/', (req, res) => {
    DatosMascotas.find({}, { __v: 0, createdAt: 0, updatedAt: 0 })
        .then((datosMascotas) => {
            res.send(datosMascotas)
        })
        .catch((error) => {
            res.status(500).send(error)
        })
});


// Nuevo -----------------------------------------------> David
// Añadiendo ruta para buscar por especie
// datosMascotasRouter.get('/:ciudad', (req, res) => {
//     const ciudad = req.params.ciudad;
//     DatosMascotas.find({ciudad: ciudad}, { __v: 0, createdAt: 0, updatedAt: 0 })
//         .then((datosMascota) => {
//             res.send(datosMascota)
//         })
//         .catch((error) => {
//             res.status(500).send(error)
//         })
// });

datosMascotasRouter.get('/filtro', (req, res) => {
    
    let filtros = {...req.query}

    console.log(filtros)

    const queryFiltros = Object.keys(filtros).map((key, i) => {
        return { [key] : filtros[key] }
    });

    let queryFinal = {};

    if (queryFiltros.length > 0){
        queryFinal = {
            $and : queryFiltros
        }
    }

    console.log(queryFiltros);

    DatosMascotas.find(
        queryFinal,
        {
            __v: 0, createdAt: 0, updatedAt: 0 
        })
        .then((datosMascota) => {
            res.status(200).send(datosMascota)
        })
        .catch((error) => {
            res.status(500).send(error)
        })
});

datosMascotasRouter.get('/:nombre', (req, res) => {
    const nombre = req.params.nombre;
    DatosMascotas.find({nombre: nombre}, { __v: 0, createdAt: 0, updatedAt: 0 })
        .then((datosMascotas) => {
            res.send(datosMascotas)
        })
        .catch((error) => {
            res.status(500).send(error)
        })
});

/* datosMascotasRouter.get('/salud', (req, res) => {
    const salud = req.params.salud;
    DatosMascotas.find({salud: salud}, { __v: 0, createdAt: 0, updatedAt: 0 })
        .then((datosMascotas) => {
            res.send(datosMascotas)
        })
        .catch((error) => {
            res.status(500).send(error)
        })
});  */


// datosMascotasRouter.get('/:ciudad/:especie/', (req, res) => {
//     const especie = req.params.especie;
//     const ciudad = req.params.ciudad;
//     DatosMascotas.find({especie: especie, tipo: tipo, peso: peso}, { __v: 0, createdAt: 0, updatedAt: 0 })
//         .then((datosMascota) => {
//             res.send(datosMascota)
//         })
//         .catch((error) => {
//             res.status(500).send(error)
//         })
// });
// // -----------------------------------------------------

module.exports = datosMascotasRouter;
